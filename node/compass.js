'use strict';

const http = require('http'),
       url = require('url'),
     mongo = require('mongojs'),
   shortid = require('shortid'),

      port = 50705,
   divider = '----------------------------------------------------',
        db = mongo('pc-user:bp1p3st0rag3m@localhost/PoliCompass', ['compasses']),

   headers = {
        json: {
            'access-control-allow-origin': 'https://policompass.com',
            'content-type': 'application/json'
        },

        plain: {
            'access-control-allow-origin': 'https://policompass.com',
            'content-type': 'text/plain'
        },

        options: {
            'access-control-allow-origin': 'https://policompass.com',
            'access-control-allow-methods': 'GET,POST,OPTIONS',
            'access-control-allow-headers': 'content-type,accept,x-requested-with',
            'access-control-max-age': 10,  // Seconds
            'content-length': 0
        }
    };


function getClientIP(req) {
    const ip = (req.headers['x-forwarded-for'] || '').split(',')[0] || req.connection.remoteAddress;
    return ip.substring(ip.lastIndexOf(':') + 1, ip.length);
}


function validateData(data) {
    let validData = false;
    if (typeof data === 'object' && typeof data.ts === 'number' && typeof data.points === 'object') {
        validData = { ts: data.ts, points: {} };
        data.arc === true && (validData.arc = true);
        for (let name of Object.keys(data.points)) {
            const point = data.points[name];
            if (typeof point === 'object' && typeof point.x === 'number' && typeof point.y === 'number') {
                const x = point.x < -10 ? -10 : point.x > 10 ? 10 : point.x,
                      y = point.y < -10 ? -10 : point.y > 10 ? 10 : point.y;
                validData.points[name] = { x, y };
                if (typeof point.color === 'string' && point.color.length < 50) {
                    validData.points[name].color = point.color;
                }
                if (typeof point.order === 'number' && point.order > -1) {
                    validData.points[name].order = point.order;
                }
                point.unnamed === true && (validData.points[name].unnamed = true);
            }
        }
    }
    return validData;
}


const server = http.createServer((req, res) => {
    const _id = url.parse(req.url, true).pathname.substring(5).replace('/', '');

    function sendResponse(code, json) {
        const resHeaders = headers[json ? 'json' : 'plain'];
        res.writeHead(code, resHeaders);
        json && res.write(json);
        res.end('\n');
        const withJson = json ? ' (with JSON)' : '';
        console.log(`Sent response: ${code} ${http.STATUS_CODES[code]}${withJson}`);
    }

    function receiveData(cb) {
        let data = '';
        req.addListener('data', chunk => {
            data += chunk;
            if (data.length > 10000) {
                console.log('Received too much data from client');
                sendResponse(400);
                req.abort();
            }
        });
        req.addListener('end', () => {
            try {
                data = JSON.parse(data);
                const validData = validateData(data);
                if (validData) {
                    console.log('Received valid data from client');
                    cb(validData);
                } else {
                    throw 'Data failed validation';
                }
            } catch (err) {
                console.log('Received invalid data from client');
                sendResponse(400);
            }
        });
    }

    console.log(divider);
    console.log('Connection from ' + getClientIP(req));

    if (req.method === 'OPTIONS') {
        console.log('Received OPTIONS request');
        res.writeHead(204, 'No Content', headers.options);
        res.end();
        console.log('Sent response: 204 No Content');

    // Client wants to POST new or updated compass
    } else if (req.method === 'POST') {
        if (shortid.isValid(_id)) {
            console.log('Received request to POST update to ' + _id);
            receiveData(data => {
                db.compasses.findOne({ _id }, (error, foundData) => {
                    if (error) {
                        console.log('Error finding data in collection');
                        sendResponse(500);
                    } else if (!foundData) {
                        console.log('Data not found in collection');
                        sendResponse(404);
                    } else {
                        console.log('Data found in collection');
                        if (foundData.arc) {
                            console.log('Data not updated: server data has archive flag');
                            sendResponse(200, JSON.stringify(foundData));
                        } else if (foundData.ts > data.ts) {
                            console.log('Data not updated: server data is newer than client data');
                            sendResponse(200, JSON.stringify(foundData));
                        } else {
                            const options = { upsert: false, multi: false };
                            db.compasses.update({ _id }, data, options, (error, updatedData) => {
                                if (error || !updatedData) {
                                    console.log('Data not updated in collection');
                                    sendResponse(500);
                                } else {
                                    console.log('Updated data in collection');
                                    sendResponse(204);  // No Content
                                }
                            });
                        }
                    }
                });
            });
        } else if (_id === 'new') {
            console.log('Received request to POST new compass');
            receiveData(data => {
                const id = shortid.generate();
                data._id = id;
                console.log('Generated ID for compass: ' + id);
                db.compasses.save(data, (error, savedData) => {
                    if (error || !savedData) {
                        console.log('Data not saved to collection');
                        sendResponse(500);  // Server Error
                    } else {
                        console.log('Data saved to collection');
                        sendResponse(201, JSON.stringify({ id }));  // Created
                    }
                });
            });
        } else { sendResponse(400); }

    // Client wants to GET an existing compass
    } else if (req.method === 'GET' && (shortid.isValid(_id) || _id === 'test')) {
        console.log('Received request to GET ' + _id);
        db.compasses.findOne({ _id }, (error, foundData) => {
            if (error) {
                console.log('Error finding data in collection');
                sendResponse(500);
            } else if (!foundData) {
                console.log('Data not found in collection');
                sendResponse(404);
            } else {
                console.log('Data found in collection');
                sendResponse(200, JSON.stringify(foundData));
            }
        });

    } else if (['DELETE', 'PUT'].includes(req.method)) {
        sendResponse(405);  // Method Not Allowed
    } else {
        sendResponse(400);  // Bad Request
    }
});


server.listen(port, 'localhost');
console.log(divider);
console.log('Listening on localhost:' + port);
