This interactive political compass is a tool for charting and comparing the political positions of people and institutions. It includes the positions of various famous people and political parties. One use is to chart your own position over time to see how it changes.

[Visit website](https://policompass.com)
