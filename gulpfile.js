'use strict';

const gulp = require('gulp'),
    concat = require('gulp-concat'),
   postcss = require('gulp-postcss'),
      sass = require('gulp-dart-sass'),
sourcemaps = require('gulp-sourcemaps'),
    terser = require('gulp-terser'),
  csswring = require('csswring');

gulp.task('styles', function () {
    const processors = [
        csswring({ removeAllComments: true })
    ];
    const srcFiles = [
        'src/styles/fonts.scss',
        'src/styles/fa.scss',
        'src/styles/anim.scss',
        'src/styles/reset.scss',
        'src/styles/layout.scss',
        'src/styles/sidebar.scss',
        'src/styles/menu.scss',
        'src/styles/point-values.scss',
        'src/styles/compass.scss',
        'src/styles/button.scss',
        'src/styles/modal.scss',
        'src/styles/bg-colors.scss'
    ];
    return gulp.src(srcFiles)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/'));
});

gulp.task('load-scripts', function () {
    const srcFiles = [
        'src/scripts/points.js',
        'src/scripts/el.js',
        'src/scripts/load.js'
    ];
    return gulp.src(srcFiles)
        .pipe(sourcemaps.init())
        .pipe(terser())
        .pipe(concat('load.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/scripts/'));
});

gulp.task('pc-scripts', function () {
    const srcFiles = [
        'src/scripts/pc.js',
        'src/scripts/svg.js',
        'src/scripts/init.js',
        'src/scripts/modals.js',
        'src/scripts/ev.js'
    ];
    return gulp.src(srcFiles)
        .pipe(sourcemaps.init())
        .pipe(terser())
        .pipe(concat('pc.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/scripts/'));
});
