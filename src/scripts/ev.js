/* globals el, Hammer, pc */
'use strict';

el.buttons.sidebar.addEventListener('click', ev => {
    //console.info('Event: click on sidebar-button');
    ev.stopPropagation();
    el.sidebar.classList.toggle('show');
});

el.actionButtons.addEventListener('click', ev => {
    ev.stopPropagation();
    const button = ev.target.id.replace('-button', '');
    if (button === 'archive') {
        //console.info('Event: click on archive-button');
        pc.showModal(pc.userData.arc ? 'archived' : 'archive');
    } else if (button === 'edit') {
        //console.info('Event: click on edit-button');
        if (el.buttons.edit.classList.contains('active')) {
            el.activePoint && pc.doneWithPoint();
        } else {
            pc.showModal('edit');
        }
    } else if (button === 'tip') {
        //console.info('Event: click on tip-button');
        pc.showModal('tip_' + (pc.tip ? pc.tip : 1));
    } else if (button) {
        //console.info('Event: click on action-button');
        pc.showModal(button);
    }
});

document.getElementById('app-title').addEventListener('click', ev => {
    //console.info('Event: click on title');
    ev.stopPropagation();
    el.sidebar.classList.toggle('show');
});

el.pointInputs.name.addEventListener('change', ev => {
    //console.info('Event: point-name changed');
    const oldName = el.activePoint.attr('id').substring(6),
          newName = pc.removeTags(ev.target.value);
    if (newName) {
        const menuItem = document.getElementById('menu-' + oldName),
                 label = menuItem.firstChild;
        el.pointInputs.name.value = newName;
        menuItem.id = 'menu-' + newName;
        label.firstChild.innerHTML = newName;
        label.querySelector('.sub-checkbox').name = newName;
        el.activePoint.attr('id', 'point-' + newName);
        el.activePoint.data([{ name: newName }]);
        if (pc.userData.points[oldName].unnamed) {
            delete pc.userData.points[oldName].unnamed;
        }
        pc.userData.points[newName] = pc.userData.points[oldName];
        delete pc.userData.points[oldName];
        el.userPoints[newName] = el.userPoints[oldName];
        delete el.userPoints[oldName];
        localStorage && localStorage[pc.id] && (localStorage[pc.id] = JSON.stringify[pc.userData]);
    } else {
        pc.userData.points[oldName].unnamed = true;
    }
});

el.pointInputs.x.addEventListener('input', ev => {
    //console.info('Event: input for point-x');
    el.activePoint.attr('cx', pc.xScale(ev.target.value));
});

el.pointInputs.y.addEventListener('input', ev => {
    //console.info('Event: input for point-y');
    el.activePoint.attr('cy', pc.yScale(ev.target.value));
});

el.pointInputs.color.addEventListener('input', ev => {
    //console.info('Event: input for point-color');
    const color = ev.target.value,
           name = el.activePoint.attr('id').substring(6),
       menuItem = document.getElementById('menu-' + name);
    //console.log('color: ' + color);
    menuItem.firstChild.lastChild.style.backgroundColor = color;
    el.activePoint.attr('fill', color);
});

el.pointInputs.remove.addEventListener('click', ev => {
    //console.info('Event: click on point-remove');
    ev.stopPropagation();
    pc.removePoint();
    pc.setTlCheckboxAndUrlParam('user');
});

el.pointInputs.done.addEventListener('click', ev => {
    //console.info('Event: click on point-done');
    ev.stopPropagation();
    pc.doneWithPoint();
    pc.setTlCheckboxAndUrlParam('user');
});

// Add click listeners to top-level menu labels
[...document.querySelectorAll('.tl-menu-label')].forEach(item => {
    item.addEventListener('click', function (ev) {
        //console.info('Event: click on tl-menu-label');
        ev.stopPropagation();
        this.parentNode.classList.toggle('expanded');
    });
});

// Add change listeners to top-level menu checkboxes
for (let name of Object.keys(el.tlCheckboxes)) {
    el.tlCheckboxes[name].addEventListener('change', function (ev) {
        //console.info('Event: tl-checkbox changed');
        ev.stopPropagation();
        pc.toggleAllPoints(name, this.checked);
        pc.setTlCheckboxAndUrlParam(name);
    });
}

// Add change listeners to sub-menu checkboxes
for (let tlMenuName of Object.keys(el.subCheckboxes)) {
    [...el.subCheckboxes[tlMenuName]].forEach(subCheckbox => {
        subCheckbox.onchange = ev => {
            pc.checkboxListener(ev, tlMenuName);
        };
    });
}

// Add mouse listeners to sub-menu labels
[...document.querySelectorAll('.sub-menu-label')].forEach(pc.addMouseoverListeners);

window.onresize = () => {
    if (!el.buttons.edit.classList.contains('active')) {
        //console.info('Event: window resize');
        el.svg.remove();
        pc.dim = pc.getDim();
        pc.renderCompass();
        pc.addUserDefined(false);
    }
};
