/* globals d3, el, pc */
'use strict';

pc.dim = pc.getDim();
pc.id = pc.validateId(location.pathname.replace(/\//g, '')) || 'new';
pc.userData = pc.defData;
el.tlCheckboxes.user.checked = false;

// Initialize d3-tip module
el.tip = d3.tip()
    .attr('class', 'd3-tip')
    .html(d => d ? d.name : '<em>Name required</em>');

d3.dispatch('selectpoint', 'deselectpoint');
document.getElementById('load-msg').remove();
pc.renderCompass();

if (localStorage) {
    if (pc.id && localStorage[pc.id] && localStorage[pc.id] !== 'undefined') {
        pc.userData = JSON.parse(localStorage[pc.id]);
        if (Object.keys(pc.userData.points).length) {
            pc.addUserDefined();
            if (location.search) {
                const user = pc.getUrlParameters();
                user && pc.togglePoints(user, true);
            }
            pc.setTlCheckboxAndUrlParam('user');
            pc.setButtons(pc.userData.arc);
        }
    }
}

if (navigator.onLine) {
    if (pc.id === 'new') {
        let showAllPeople = true;
        pc.tip = 1;
        el.buttons.tip.classList.add('show');
        if (location.search) {
            const { people, parties } = pc.getUrlParameters();
            if (people || parties) {
                showAllPeople = false;
                pc.togglePoints({ people, parties }, true, true);
            }
        }
        if (showAllPeople) {
            pc.toggleAllPoints('people', true);
            pc.setTlCheckboxAndUrlParam('people');
            document.getElementById('menu-people').classList.add('expanded');
        }
    } else {
        pc.fetchCompass();
    }
}
pc.resetPointValues();
pc.addNewPointMenuItem();

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js')
        /* .then(reg => {
            console.info('Service worker registered with scope: ', reg.scope);
        }) */
        .catch(err => {
            console.error('Service worker not registered: ', err);
        });
}
