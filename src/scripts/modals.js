/* globals QRCode, el, pc */
'use strict';

const modals = {

    about: {
        content: `<h3>PoliCompass is a tool for charting political positions.</h3>
            <p>It was inspired by <a href="https://politicalcompass.org" rel="noopener" target="_blank">politicalcompass.org</a>, which is the recommended destination for detailed information on this system.</p>
            <p class="smaller">This site is not affiliated with the site above. No guarantee is given about the accuracy of any information presented on either site.</p>
            <p class="smaller">Please email <em>contact</em> at this domain to report bugs or suggest datasets.</p>`
    },

    edit: {
        content: `<h3>To edit a user-defined point:</h3>
            <p>Select a point by tapping it or long-pressing a menu item.</p>
            <p>Simply drag a point to change its position.</p>
            <p class="smaller">Changes are automatically saved. To leave edit mode, tap the edit or save button.</p>`
    },

    fetchError: {
        content: '<h3>Hmm... Something weird happened when trying to fetch the requested compass.</h3>'
    },

    sendError: {
        content: `<h3>There was a problem communicating with the server.</h3>
            <p>Your changes have not been uploaded. Another attempt will be made later.</p>`
    },

    notFound: {
        content: '<h3 class="one-liner">Requested compass does not exist!</h3>'
    },

    archive: {
        content: `<h3>Do you want to archive this compass?</h3>
            <p>This will permanently freeze all user-defined points.</p>
            <button class="archive"><i class="fa fa-archive"></i>Archive</button>
            <button class="cancel"><i class="fa fa-close"></i>Cancel</button>
            <p class="smaller">Archived compasses can still be forked.</p>`,

        afterCreate: modal => {
            modal.modalElem().addEventListener('click', ev => {
                modal.close();
                if (ev.target.classList.contains('archive')) {
                    pc.archiveCompass();
                }
            });
        }
    },

    archived: {
        content: `<h3>This compass is archived.</h3>
            <p>Fork this compass if you want to makes changes.</p>`,
    },

    conflict: {
        content: `<h3>There have been other changes to this compass that are more recent than yours.</h3>
            <button class="fork"><i class="fa fa-code-fork"></i>Keep my changes and fork this compass</button>
            <button class="discard"><i class="fa fa-cloud-download"></i>Discard my changes and show updates to this compass</button>`,

        afterCreate: modal => {
            modal.modalElem().addEventListener('click', ev => {
                localStorage && (localStorage[pc.id] = pc.serverData);
                if (ev.target.classList.contains('fork')) {
                    pc.forkCompass();
                } else /* if (ev.target.classList.contains('discard')) */ {
                    pc.userData = pc.serverData;
                    pc.clearUserDefined();
                    pc.addUserDefined();
                }
                pc.serverData = null;
                modal.close();
            });
        },

        afterClose: modal => {
            modal.destroy();
            el.modals.conflict = null;
        }
    },

    is_arc: {
        content: `<h3>This compass was archived before your changes could be saved.</h3>
            <button class="fork"><i class="fa fa-code-fork"></i>Keep my changes and fork this compass</button>
            <button class="discard"><i class="fa fa-cloud-download"></i>Discard my changes and show archived compass</button>`,

        // This modal uses the same afterCreate method as the conflict modal above

        afterClose: modal => {
            modal.destroy();
            el.modals.is_arc = null;
        }
    },

    fork: {
        content: `<h3>Do you want to fork this compass?</h3>
            <p>This will assign the compass a new web address.</p>
            <button class="fork"><i class="fa fa-code-fork"></i>Fork</button>
            <button class="cancel"><i class="fa fa-close"></i>Cancel</button>
            <p class="smaller">If you are offline this action will occur later.</p>`,

        afterCreate: modal => {
            modal.modalElem().addEventListener('click', ev => {
                modal.close();
                if (ev.target.classList.contains('fork')) {
                    pc.forkCompass();
                }
            });
        }
    },

    new: {
        content: `<h3>Do you want create a new compass?</h3>
            <p>This will remove all user-defined points from the display.</p>
            <button class="new"><i class="fa fa-file"></i>New</button>
            <button class="cancel"><i class="fa fa-close"></i>Cancel</button>
            <p class="smaller">The current compass will not be deleted from the server.</p>`,

        afterCreate: modal => {
            modal.modalElem().addEventListener('click', ev => {
                if (ev.target.classList.contains('new')) {
                    pc.newCompass();
                }
                modal.close();
            });
        }
    },

    open: {
        content: `<h3 id="open-compass-title">Enter a compass ID:</h3>
            <div id="stored-compasses"></div>
            <input id="open-compass-input" type="text">
            <button id="open-compass-button" disabled="true">Open</button>`,

        afterCreate: modal => {
            let buttons = '';
            for (let id of Object.keys(localStorage)) {
                buttons += `<button class="open" id="open-${id}">${id}</button>`;
            }
            if (buttons) {
                document.getElementById('open-compass-title').textContent = 'Choose a compass or enter an ID:';
                document.getElementById('stored-compasses').innerHTML = buttons;
            }
            const openInput = document.getElementById('open-compass-input'),
                 openButton = document.getElementById('open-compass-button');
            openButton.addEventListener('click', () => {
                modal.modalElem().innerHTML = '<h3 class="one-liner">Just a moment...</h3>';
                history.replaceState('', '', '/');
                location.pathname = openInput.value + '/';
            });
            openInput.addEventListener('input', ev => {
                openButton.disabled = !pc.validateId(ev.target.value);
            });
            modal.modalElem().addEventListener('click', ev => {
                if (ev.target.classList.contains('open') && ev.target.id) {
                    modal.modalElem().innerHTML = '<h3 class="one-liner">Just a moment...</h3>';
                    history.replaceState('', '', '/');
                    location.pathname = ev.target.id.substring(5) + '/';
                }
            });
        }
    },

    saved: {
        content: `<h3>Your changes have been saved.</h3>
            <p>This compass can be accessed at:</p>
            <p id="url"></p>
            <div id="qr-code"></div>
            <p class="smaller">Anyone you share this address with can make changes to this compass.</p>`,

        afterCreate: pc.setShareUrl,

        afterClose: modal => {
            pc.showButtons('new', 'share', 'fork', 'archive', 'edit');
            modal.destroy();
            el.modals.saved = null;
        }
    },

    share: {
        content:`<p style="margin-top: 0">This compass can be accessed at:</p>
            <p class="larger" id="url"></p>
            <div id="qr-code"></div>
            <p class="smaller">${pc.userData.arc || pc.isArc ? 'This compass is archived so no changes are allowed. However it can still be forked.' : 'Anyone you share this address with can make changes to this compass.'}</p>`,

        afterCreate: pc.setShareUrl
    },

    tip_1: {
        content: `<h3>To add a point you'll need position coordinates.</h3>
            <p>To get your position, take the test at <a href="https://politicalcompass.org/test" rel="noopener" target="_blank">politicalcompass.org/test</a></p>
            <p>Your coordinates will be in the subsequent analysis.</p>
            <p class="smaller"><label><input class="no-tip" id="no-tip-1" type="checkbox">Do not show this tip again</label></p>`,

        afterCreate: () => {
            document.getElementById('no-tip-' + pc.tip).addEventListener('change', ev => {
                ev.target.checked ? pc.tip++ : pc.tip--;
            });
        }
    },

    tip_2: {
        content: `<h3>Share your compass with your family and friends.</h3>
            <p>Select <i class="fa fa-paper-plane"></i> under a compass with user-defined points to view a link and QR code.</p>
            <p>If you share an unarchived compass the recipient will be able to add their own points.</p>
            <p class="smaller"><label><input class="no-tip" id="no-tip-2" type="checkbox">Do not show this tip again</label></p>`
    },

    tip_3: {
        content: `<h3>Rearrange the menu order of user-defined points.</h3>
            <p>Long-press the menu item until it is highlighted and then drag it to another position in the list.</p>
            <p>Long-pressing also selects the point for editing.</p>
            <p class="smaller"><label><input class="no-tip" id="no-tip-3" type="checkbox">Do not show this tip again</label></p>`
    },

    tip_4: {
        content: `<h3>Plot your political position over time.</h3>
            <p>To visualize how your political position changes over time, take the test periodically, each time adding the new result to the same compass.</p>
            <p class="smaller"><label><input class="no-tip" id="no-tip-3" type="checkbox">Do not show this tip again</label></p>`,

            afterClose: () => {
                el.buttons.tip.classList.remove('show');
            }
    }

};

modals.is_arc.afterCreate = modals.conflict.afterCreate;
modals.tip_2.afterCreate = modals.tip_1.afterCreate;
modals.tip_3.afterCreate = modals.tip_1.afterCreate;
modals.tip_4.afterCreate = modals.tip_1.afterCreate;
