/* globals pc */
'use strict';

// ChildNode.remove() method polyfill for IE11
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function () {
        this.parentNode && this.parentNode.removeChild(this);
    };
}

(function () {
    function loadScript(src, done) {
        const script = document.createElement('script');
        script.src = '/scripts/' + src;
        done && (script.onload = done);
        script.onerror = () => {
            throw new Error('Failed to load ' + src);
        };
        document.body.appendChild(script);
    }

    function loadAxiosThenPc() {
        loadScript('axios.min.js', () => {
            loadScript('pc.min.js', () => {
                loadScript('hammer.min.js', pc.makeSwipable);
                loadScript('sortable.min.js', pc.makeItemsSortable);
                loadScript('qrcode.min.js', () => {
                    window.QRCode && pc.destroyModal('share');
                });
            });
        });
    }

    if (typeof CSS !== 'object' || !CSS.escape) {
        loadScript('css.escape.min.js');
    }
    if (!Array.from || !Promise || !Symbol) {
        loadScript('polyfill.min.js', loadAxiosThenPc);
    } else {
        loadAxiosThenPc();
    }
})();
