'use strict';

const el = {

    // DOM elements
    main:           document.querySelector('main'),
    topBar:         document.getElementById('top-bar'),
    sidebar:        document.getElementById('sidebar'),
    actionButtons:  document.getElementById('action-buttons'),
    //menu:           document.getElementById('menu'),
    //appTitle:       document.getElementById('app-title'),
    //tlMenuLabels:   document.querySelectorAll('.tl-menu-label'),
    //subMenuLabels:  document.querySelectorAll('.sub-menu-label'),
    userSubMenu:    document.querySelector('#menu-user > .sub-menu'),

    tlCheckboxes:   {
        people:     document.querySelector('#menu-people .tl-checkbox'),
        parties:    document.querySelector('#menu-parties .tl-checkbox'),
        user:       document.querySelector('#menu-user .tl-checkbox')
    },

    subCheckboxes: {
        people:     document.querySelectorAll('#menu-people .sub-checkbox'),
        parties:    document.querySelectorAll('#menu-parties .sub-checkbox'),
        user:       document.getElementsByClassName('user-checkbox')
    },

    buttons: {
        sidebar:    document.getElementById('sidebar-button'),
        archive:    document.getElementById('archive-button'),
        edit:       document.getElementById('edit-button'),
        fork:       document.getElementById('fork-button'),
        share:      document.getElementById('share-button'),
        new:        document.getElementById('new-button'),
        open:       document.getElementById('open-button'),
        tip:        document.getElementById('tip-button'),
        about:      document.getElementById('about-button')
    },

    pointInputs: {
        name:       document.getElementById('point-name'),
        x:          document.getElementById('point-x'),
        y:          document.getElementById('point-y'),
        color:      document.getElementById('point-color'),
        remove:     document.getElementById('point-remove'),
        done:       document.getElementById('point-done')
    },

    // d3 selections
    labels:         {},    // Compass labels
    quadrants:      {},    // Compass quadrants
    userPoints:     {},    // User-defined points

    // picoModal objects
    modals:         {}

};
