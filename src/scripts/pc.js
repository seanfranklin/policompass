/* globals d3, axios, Hammer, picoModal, QRCode, Sortable, modals, points, el */
'use strict';

const pc = {

    rem: parseInt(window.getComputedStyle(document.body).fontSize),

    defPoint: {
        x: 0, y: 0,
        color: '#111111'
    },

    defData: {
        ts: 100000000000,
        points: {}
    },


    /**
     * Calculates the basic dimension used to build the compass.
     */

    getDim() {
        //console.log('getDim()');
        return Math.min(el.main.clientWidth, el.main.clientHeight) / 10;
    },


    /**
     * Sanitizes a string by removing HTML tags and comments.
     * http://stackoverflow.com/queslastChild566/sanitize-rewrite-html-on-the-client-side
     */

    removeTags(str = '') {
        //console.log(`removeTags(${str})`);
        const attr = '(?:[^"\'>]|"[^"]*"|\'[^\']*\')*',
               tag = new RegExp(`<(?:!--(?:(?:-*[^->])*--+|-?)|script\\b${attr}>[\\s\\S]*?</script\\s*|style\\b${attr}>[\\s\\S]*?</style\\s*|/?[a-z]${attr})>`, 'gi');
        let oldStr;
        do {
            oldStr = str;
            str = str.replace(tag, '');
        } while (str !== oldStr);
        return str.replace(/</g, '');
    },


    /**
     * Rounds a number to the specified number of decimal places.
     * This function is an alternative to parseFloat(num.toFixed(precision))
     */

    round(num, precision) {
        //console.log(`round(${num}, ${precision})`);
        const multiplier = Math.pow(10, precision || 0);
        return Math.round(num * multiplier) / multiplier;
    },


    /**
     * Checks if any compasses other than current one are in localStorage.
     * @return {boolean}
     */

    isStoredCompasses() {
        //console.log('isStoredCompasses()');
        if (localStorage) {
            const threshold = localStorage[pc.id] ? 1 : 0;
            if (localStorage.length > threshold) {
                return true;
            }
        }
        return false;
    },


    /**
     * Hides the specified action buttons.
     * @param {string} buttons - Series of buttons
     */

    hideButtons(...buttons) {
        //console.log(`hideButtons(${buttons})`);
        buttons.forEach(button => {
            el.buttons[button].classList.remove('show');
        });
    },


    /**
     * Shows the specified action buttons.
     * @param {string} buttons - Series of buttons
     */

    showButtons(...buttons) {
        //console.log(`showButtons(${buttons})`);
        buttons.forEach(button => {
            button && el.buttons[button].classList.add('show');
        });
    },


    /**
     * Sets action button visibility and titles for existing compass.
     * @param {boolean} [isArc] - Compass is archived
     */

    setButtons(isArc) {
        //console.log(`setButtons(${isArc})`);
        const edit = isArc ? null : 'edit',
           archive = isArc || Object.keys(pc.userData.points).length > 0 ? 'archive' : null;
        pc.showButtons('open', 'new', 'share', 'fork', archive, edit);
        if (edit) {
            el.buttons.archive.classList.remove('active');
            el.buttons.archive.title = 'Archive';
        } else if (archive) {
            el.buttons.archive.classList.add('active');
            el.buttons.archive.title = 'Archived';
        }
    },


    /**
     * Gets number and color input values in point-values.
     * @return {object} point
     */

    getPointValues() {
        //console.log('getPointValues()');
        const point = {
            x:     parseFloat(el.pointInputs.x.value),
            y:     parseFloat(el.pointInputs.y.value),
            color: el.pointInputs.color.value
        };
        el.pointInputs.name.value === '' && (point.unnamed = true);
        return point;
    },


    /**
     * Sets input values in point-values according to passed object.
     */

    setPointValues(name, point) {
        //console.log(`setPointValues(${name})`);
        const merged = Object.assign({}, pc.defPoint, point);
        el.pointInputs.name.value = (point.unnamed ? '' : name) || '';
        el.pointInputs.x.value = merged.x;
        el.pointInputs.y.value = merged.y;
        el.pointInputs.color.value = merged.color;
    },


    /**
     * Resets all input values in point-values to their defaults.
     */

    resetPointValues() {
        //console.log('resetPointValues()');
        el.pointInputs.name.value = '';
        el.pointInputs.x.value = pc.defPoint.x;
        el.pointInputs.y.value = pc.defPoint.y;
        el.pointInputs.color.value = pc.defPoint.color;
    },


    /**
     * Sets the appearance and point-values for a point.
     * @param {string} name - Point’s name
     */

    setActivePoint(name, elem = document.getElementById('point-' + name)) {
        //console.log(`setActivePoint(${name})`);
        el.buttons.edit.classList.add('show', 'active');
        el.buttons.edit.title = 'Editing...';
        el.activePoint && el.activePoint.classed('active', false);
        el.activePoint = d3.select(elem).classed('active', true);  //.raise()
        const point = pc.userData.points[name] ||
            Object.assign({}, pc.defPoint, { unnamed: true});
        pc.setPointValues(name, point);
    },


    /**
     * Move a point on the compass and update point-values coordinates.
     * Note: Possibly better to bind directly to el.userPoints.data
     */

    dragPoint() {
        //console.log('pc.dragPoint()');
        if (pc.isArc || pc.userData.arc) { return; }
        //console.info('Event: start drag on ' + this.id);
        pc.isDragging = true;
        const name = this.id.substring(6),
            prevName = el.activePoint ? el.activePoint.attr('id').substring(6) : null;
        if (!el.activePoint) {
            pc.setActivePoint(name, this);
        } else if (prevName !== name) {
            pc.storePoint(prevName, pc.getPointValues(), true);
            pc.setActivePoint(name, this);
        }
        el.tip.hide(el.activePoint);
        const dragged = d => {
                //console.info(`Event: drag ${this.id} to ${d3.event.x}, ${d3.event.y}`);
                el.activePoint
                    .attr('cx', d3.event.x)
                    .attr('cy', d3.event.y);
                el.pointInputs.x.value = pc.round(pc.xCoord(d3.event.x), 2);
                el.pointInputs.y.value = pc.round(pc.yCoord(d3.event.y), 2);
            },
            ended = () => {
                //console.info('Event: end drag on ' + this.id);
                pc.isDragging = false;
            };
        d3.event.on('drag', dragged).on('end', ended);
        el.topBar.classList.add('show');
    },


    /**
     * Adds a new point (SVG circle) to the compass.
     * @return {Object} d3 object
     */

    addPoint(name, point = {}, show = false) {
        //console.log(`addPoint(${name})`);
        const merged = Object.assign({}, pc.defPoint, point);
        return el.compass.append('circle')
            .classed('point', true)
            .attr('id', 'point-' + name)
            .attr('cx', pc.xScale(merged.x))
            .attr('cy', pc.yScale(merged.y))
            .attr('r', pc.rem * 0.5)
            .attr('fill', merged.color)
            .on('mouseover', function (d) { pc.isDragging || el.tip.show(d); })
            .on('mouseout', el.tip.hide)
            .on('selectpoint', el.tip.show)
            .on('deselectpoint', el.tip.hide)
            .call(d3.drag().on('start', pc.dragPoint))
            .classed('show', show);
    },


    reorderPoints(min = 0, max = Object.keys(pc.userData.points).length - 1) {
        //console.log(`reorderPoints(${min}, ${max})`);
        if (window.Sortable) {
            const menuItems = pc.sortable.el.querySelectorAll('.sub-menu-item:not(.add-item)');
            for (let i = min; i <= max; i++) {
                pc.userData.points[menuItems[i].id.substring(5)].order = i;
                console.log(menuItems[i].id.substring(5) + ': ' +
                    pc.userData.points[menuItems[i].id.substring(5)].order);
            }
        } else {
            // A point has been deleted so decrement order of subsequent points
            Object.keys(pc.userData.points)
                .filter(name => pc.userData.points[name].order > min)
                .forEach(name => {
                    pc.userData.points[name].order--;
                });
        }
    },


    removePoint(name = el.activePoint.attr('id').substring(6)) {
        //console.log(`removePoint(${name})`);
        let order = -1;
        if (pc.userData.points[name]) {
            order = pc.userData.points[name].order;
            delete pc.userData.points[name];
            pc.userData.ts = Date.now();
            pc.storeUserData();
        }
        el.topBar.classList.remove('show');
        el.activePoint.classed('show', false);
        /* if (window.Hammer && pc.hammer[name]) {
            pc.hammer[name].destroy();
            delete pc.hammer[name];
        } */
        document.getElementById('menu-' + name).remove();
        order > -1 && pc.reorderPoints(order);
        navigator.onLine && !pc.isWaitingForId && pc.id !== 'new' && pc.sendCompass();
        setTimeout(() => {
            el.activePoint.remove();
            el.activePoint = null;
            el.buttons.edit.classList.remove('active');
            el.buttons.edit.title = 'Edit';
        }, 200);
    },


    getPointOrder(name) {
        //console.log(`getPointOrder(${name})`);
        const menuItems = Array.from(el.userSubMenu.querySelectorAll('.sub-menu-item:not(.add-item)')),
                  order = menuItems.findIndex(menuItem => menuItem.id.substring(5) === name);
        return order > -1 ? order : menuItems.length;
    },


    /**
     * Stores user data in localStorage (if supported) under the current compass ID.
     * @param {object} [data] - Default is pc.userData
     */

    storeUserData(data = pc.userData) {
        //console.log(`storeUserData(${data})`);
        localStorage && (localStorage[pc.id] = JSON.stringify(data));
    },


    /**
     * Stores point coordinates and color in the userData object.
     * @param {string} name - Point's name
     */

    storePoint(name, point = { unnamed: true }, isStoreLocal = true) {
        //console.log(`storePoint(${name})`);
        const merged = Object.assign({}, pc.defPoint, point);
        if (pc.userData.points[name]) {
            merged.order = pc.userData.points[name].order;
        }
        if (typeof merged.order !== 'number' || merged.order < 0) {
            merged.order = pc.getPointOrder();
        }
        const storedPoint = pc.userData.points[name];
        if (!storedPoint || storedPoint.x !== merged.x || storedPoint.y !== merged.y ||
            storedPoint.color !== merged.color || storedPoint.order !== merged.order) {
            pc.userData.points[name] = merged;
            pc.userData.ts = Date.now();
            isStoreLocal && pc.storeUserData();
        }
    },


    /**
     * Manages the process of switching out of edit mode.
     */

    doneWithPoint() {
        //console.log('doneWithPoint()');
        const name = el.activePoint.attr('id').substring(6);
        pc.storePoint(name, pc.getPointValues(), true);
        el.topBar.classList.remove('show');
        el.activePoint.classed('active', false);
        el.activePoint = null;
        el.buttons.archive.classList.add('show');
        el.buttons.edit.classList.remove('active');
        el.buttons.edit.title = 'Edit';
        navigator.onLine && !pc.isWaitingForId && pc.sendCompass();
    },


    /**
     * Adds mouseover and mouseout event listeners to a sub-menu-label.
     */

    addMouseoverListeners(label) {
        //console.log('addMouseoverListeners()');
        label.addEventListener('mouseover', function (ev) {
            //console.info('Event: mouseover sub-menu-label');
            ev.stopPropagation();
            const checkbox = this.querySelector('.sub-checkbox');
            checkbox.checked && el.compass.select('#point-' + CSS.escape(checkbox.name))
                .dispatch('selectpoint', { bubbles: false });
        });
        label.addEventListener('mouseout', function (ev) {
            //console.info('Event: mouseout sub-menu-label');
            ev.stopPropagation();
            const checkbox = this.querySelector('.sub-checkbox');
            checkbox.checked && el.compass.select('#point-' + CSS.escape(checkbox.name))
                .dispatch('deselectpoint', { bubbles: false });
        });
    },


    /**
     * Called by a change event on a sub-checkbox.
     */

    checkboxListener(ev, tlMenuName = 'user') {
        //console.info('Event: sub-checkbox changed');
        ev.stopPropagation();
        const checkbox = ev.target;
        el.compass.select('#point-' + CSS.escape(checkbox.name))
            .classed('show', checkbox.checked)
            .dispatch(checkbox.checked ? 'selectpoint' : 'deselectpoint', { bubbles: false });
        pc.setTlCheckboxAndUrlParam(tlMenuName);
    },


    getAncestorEl(elem, className) {
        //console.log(`getAncestorEl(${})`);
        while (!elem.classList.contains(className)) {
            elem = elem.parentNode;
        }
        return elem;
    },


    /**
     * Manages adding a new point and associated menu item.
     */

    addNewPointEvent(ev) {
        //console.log('addNewPointEvent()');
        ev.preventDefault();
        ev.stopPropagation();
        console.info('Event: click on sub-menu-label to add-item');

        if (el.activePoint) {
            const name = el.activePoint.attr('id').substring(6);
            pc.storePoint(name, pc.getPointValues(), true);
            el.activePoint.classed('active', false);
        } else {
            el.buttons.edit.classList.add('show', 'active');
            el.buttons.edit.title = 'Editing...';
        }

        const name = (Math.random() * 10000).toFixed(0),
          menuItem = pc.getAncestorEl(ev.target, 'add-item');
        menuItem.id = 'menu-' + name;
        menuItem.classList.remove('add-item');

        const label = menuItem.firstChild;
        label.firstChild.innerHTML = '<em>Name required</em>';
        label.removeEventListener('click', pc.addNewPointEvent);

        const subCheckbox = label.querySelector('.sub-checkbox');
        subCheckbox.classList.add('user-checkbox');
        subCheckbox.onchange = pc.checkboxListener;
        el.subCheckboxes.user.length === 1 && (el.tlCheckboxes.user.checked = true);

        pc.resetPointValues();
        el.pointInputs.name.focus();
        el.topBar.classList.add('show');
        el.userPoints[name] = pc.addPoint(name, {}, true);
        el.userPoints[name].append('rect');
        el.userPoints[name].classed('active', true);
        el.activePoint = el.userPoints[name];

        const customCheckbox = menuItem.firstChild.lastChild ||
            el.subCheckboxes.user.namedItem(name);
        customCheckbox.classList.remove('fa-plus', 'fa');
        customCheckbox.style.backgroundColor = pc.defPoint.color;

        pc.storePoint(name, undefined, false);
        pc.setTlCheckboxAndUrlParam('user');
        pc.addNewPointMenuItem();
        pc.tip === 1 && pc.showModal('tip_1');
    },


    /**
     * Makes sidebar toggleable via swiping action.
     * Uses the Hammer.js library (https://github.com/hammerjs/hammer.js).
     */

    makeSwipable() {
        //console.log('makeSwipable()');
        if (window.Hammer) {
            pc.hammer = pc.hammer || {};
            pc.hammer.document = new Hammer.Manager(document);
            pc.hammer.document.add(new Hammer.Swipe({ direction: Hammer.DIRECTION_HORIZONTAL }));
            pc.hammer.document.on('swiperight', ev => {
                //console.info('Event: swiperight on document');
                el.sidebar.classList.add('show');
            });
            pc.hammer.sidebar = new Hammer.Manager(el.sidebar);
            pc.hammer.sidebar.add(new Hammer.Swipe({ direction: Hammer.DIRECTION_HORIZONTAL }));
            pc.hammer.sidebar.on('swipeleft', ev => {
                //console.info('Event: swipeleft on sidebar');
                el.sidebar.classList.remove('show');
            });
        }
    },


    /**
     * Makes user-defined menu items vertically sortable by dragging.
     * Uses the Sortable.js library (https://github.com/RubaXa/Sortable).
     */

    makeItemsSortable() {
        //console.log('makeItemsSortable()');
        if (window.Sortable) {
            pc.sortable = Sortable.create(el.userSubMenu, {
                draggable: '.sub-menu-item',
                filter:    '.add-item',
                preventOnFilter: false,
                animation: 150,
                delay:     700,

                onChoose(ev) {
                    //console.info('Event: choose sub-menu-item');
                    function preventCheckboxOff(e) {
                        e.preventDefault();
                        ev.item.removeEventListener('click', preventCheckboxOff);
                    }
                    ev.item.addEventListener('click', preventCheckboxOff);
                    if (el.activePoint) {
                        const name = el.activePoint.attr('id').substring(6);
                        pc.storePoint(name, pc.getPointValues(), true);
                    }
                    if (!pc.isArc) {
                        pc.setActivePoint(ev.item.id.substring(5));
                        el.topBar.classList.add('show');
                        ev.item.querySelector('.sub-checkbox').checked = true;
                        el.activePoint.classed('show', true);
                    }
                },

                onEnd(ev) {
                    //console.info('Event: end drag on sub-menu-item');
                    if (ev.oldIndex !== ev.newIndex) {
                        const min = Math.min(ev.oldIndex, ev.newIndex),
                              max = Math.max(ev.oldIndex, ev.newIndex);
                        pc.reorderPoints(min, max);
                        pc.storeUserData();
                        navigator.onLine && !pc.isWaitingForId && pc.id !== 'new' && pc.sendCompass();
                    }
                }
            });
        }
    },


    /**
     * Adds a user-definable menu item to the sub-menu.
     * @param {string} [name]
     */

    addMenuItem(name = '', color = '#aeb2b7', isNew = true, isUnnamed = false) {
        //console.log(`addMenuItem(${name})`);
        const item = document.createElement('li'),
             label = document.createElement('label'),
          itemName = isUnnamed ? '<em>Name required</em>' : (name ? name : '<em>Add new point</em>'),
              html =

`<span class="menu-item-name">${itemName}</span>
    <input class="menu-checkbox sub-checkbox${isNew ? '' : ' user-checkbox'}" type="checkbox" name="${name || 'add'}" checked>
    <span class="custom-checkbox pull-right${isNew ? ' fa fa-plus' : ''}" style="background-color: ${color}">
</span>`;

        name && (item.id = 'menu-' + name);
        item.classList.add('menu-item', 'sub-menu-item');
        isNew && item.classList.add('add-item');
        label.classList.add('menu-label', 'sub-menu-label');
        label.innerHTML = html;
        pc.addMouseoverListeners(label);
        isNew && label.addEventListener('click', pc.addNewPointEvent);
        item.insertAdjacentElement('afterbegin', label);
        el.userSubMenu.insertAdjacentElement('beforeend', item);

        if (!isNew) {
            const checkbox = label.querySelector('.sub-checkbox') ||
                el.subCheckboxes.user.namedItem(name);
            checkbox.onchange = pc.checkboxListener;
        }
    },


    /**
     * Adds an 'add new point' item to the sub-menu if one doesn't already exist.
     */

    addNewPointMenuItem() {
        //console.log('addNewPointMenuItem()');
        pc.userData.arc || pc.isArc || el.userSubMenu.querySelector('.add-item') || pc.addMenuItem();
    },


    getUrlParameters() {
        //console.log('getUrlParameters()');
        const search = location.search.substring(1);
        return search ? JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g, '":"') + '"}',
            (key, value) => key === '' ? value : decodeURIComponent(value)) : null;
    },


    updateUrlParameter(param, value){
        let arr = location.href.split('?'),
            and = '', newQuery = '';
        const baseUrl = arr[0].substr(-1) === '/' ? arr[0] : arr[0] + '/',
                query = arr[1];
        if (query) {
            arr = query.split('&');
            for (let i = 0; i < arr.length; i++) {
                if(arr[i].split('=')[0] != param) {
                    newQuery += and + arr[i];
                    and = '&';
                }
            }
        }
        value && (newQuery += and + param + '=' + encodeURIComponent(value).replace(/%2C/g, ','));
        return baseUrl + '?' + newQuery;
    },


    /**
     * Sets checked value of a top-level checkbox according to its sub-checkboxes.
     * Also sets the corresponding URL query parameter.
     * @param {string} name
     */

    setTlCheckboxAndUrlParam(name) {
        //console.log(`setTlCheckboxAndUrlParam(${name})`);
        const subCheckboxes = el.subCheckboxes[name],
            checked = [...subCheckboxes].filter(checkbox => checkbox.checked),
            isAll = subCheckboxes.length && subCheckboxes.length === checked.length;

        el.tlCheckboxes[name].checked = isAll;
        const url = pc.updateUrlParameter(name, isAll ? 'all' :
            checked.reduce((str, checkbox) => str + (str ? ',' : '') + checkbox.name, ''));
        history.replaceState('', '', url);
    },


    togglePoint(checkbox, isChecked) {
        //console.log(`togglePoint(${checkbox}, ${isChecked})`
        checkbox.checked = isChecked;
        el.compass.select('#point-' + CSS.escape(checkbox.name))
            .classed('show', isChecked)
            .dispatch('deselectpoint', { bubbles: false });
    },


    toggleAllPoints(tlName, isChecked) {
        //console.log(`toggleAllPoints(${tlName}, ${isChecked})`);
        for (let checkbox of el.subCheckboxes[tlName]) {
            pc.togglePoint(checkbox, isChecked);
        }
        el.tlCheckboxes[tlName].checked = isChecked;
    },


    // Must be passed an object of valid tlName keys with strings as values
    togglePoints(points, isChecked, isToggleExpanded) {
        //console.log(`togglePoints(${isChecked}, ${isToggleExpanded})`);
        const tlNames = Object.keys(points),
              counts = [], invalidPointIn = [];
        for (let tlName of tlNames) {
            let count = 0;
            if (points[tlName] === 'all') {
                pc.toggleAllPoints(tlName, isChecked);
                count = el.subCheckboxes[tlName].length;
            } else {
                let pointNames = points[tlName] ? points[tlName].toLowerCase().split(',') : [];
                pointNames.length && [...el.subCheckboxes[tlName]].forEach(checkbox => {
                    if (pointNames.includes(checkbox.name.toLowerCase())) {
                        pc.togglePoint(checkbox, isChecked);
                        count++;
                    }
                });
                count < pointNames.length && invalidPointIn.push(tlName);
            }
            counts.push(count);
        }
        if (isToggleExpanded) {
            if (isChecked) {
                const tlName = tlNames[counts.indexOf(Math.max(...counts))];
                document.getElementById('menu-' + tlName).classList.add('expanded');
            } else {
                counts.forEach((count, index) => {
                    count === 0 && document.getElementById('menu-' + tlNames[index]).classList.remove('expanded');
                });
            }
        }
        invalidPointIn.forEach(tlName => {
            pc.setTlCheckboxAndUrlParam(tlName);
        });
    },


    /**
     * Removes all user-defined points and associated menu items.
     * @param {string} buttons - Series of buttons
     */

    clearUserDefined() {
        //console.log('clearUserDefined()');
        document.querySelector('#menu-user .sub-menu').innerHTML = '';
        el.activePoint = null;
        const points = Object.keys(el.userPoints);
        for (let name of points) {
            el.userPoints[name].remove();
        }
        el.userPoints = {};
    },


    /**
     * Adds all points defined in the userData object to the compass and menu.
     * @param {boolean} [addMenuItem] - Default is true
     * @param {boolean} [addPoint] - Default is true
     */

    addUserDefined(addMenuItem = true, addPoint = true) {
        //console.log(`addUserDefined(${addMenuItem}, ${addPoint})`);
        const names = Object.keys(pc.userData.points) || [],
              orderedNames = [];
        for (let i = 0; i < names.length; i++) {
            const order = pc.userData.points[names[i]].order,
                isValid = typeof order === 'number' && order > -1;
            orderedNames[isValid ? order : i] = names[i];
        }
        orderedNames.forEach(name => {
            addMenuItem && pc.addMenuItem(name, pc.userData.points[name].color, false,
                pc.userData.points[name].unnamed);
            if (addPoint) {
                el.userPoints[name] = pc.addPoint(name, pc.userData.points[name], true);
                el.userPoints[name].data([{ name }]);
            }
        });
    },


    /**
     * Makes a d3 SVG axis.
     * @param {object} selection
     */

    makeAxis(selection) {
        //console.log(`makeAxis(${selection.attr('id')})`);
        const axis = selection.attr('id').charAt(0),
               isX = (axis === 'x'),
            orient = isX ? 'axisBottom' : 'axisRight',
             scale = isX ? 'xScale' : 'yScale';

        selection.call(d3[orient](pc[scale])
            .ticks(10)
            .tickPadding(pc.rem * 0.8)
            .tickSizeInner(pc.dim * 6)
            .tickSizeOuter(0)
            .tickFormat(d => d > 0 ? '+' + d : d));
    },


    /**
     * Sets the functions that determine the x and y scales on the compass.
     */

    setScales() {
        //console.log('setScales()');
        pc.xScale = d3.scaleLinear()
            .domain([-10, 10])
            .range([pc.dim, pc.dim * 7]);

        pc.yScale = d3.scaleLinear()
            .domain([10, -10])
            .range([pc.dim, pc.dim * 7]);

        pc.xCoord = d3.scaleLinear()
            .domain([pc.dim, pc.dim * 7])
            .range([-10, 10]);

        pc.yCoord = d3.scaleLinear()
            .domain([pc.dim, pc.dim * 7])
            .range([10, -10]);
    },


    /**
     * Sets the compass URL and QR code in the corresponding elements.
     * Uses the QRCode.js library (https://github.com/davidshimjs/qrcodejs/).
     */

    setShareUrl() {
        //console.log('setShareUrl()');
        document.getElementById('url').innerHTML = 'policompass.com/' + pc.id;
        window.QRCode && new QRCode(document.getElementById('qr-code'), {
            text:       'https://policompass.com/' + pc.id,
            width:      128,
            height:     128,
            colorDark:  '#111',
            colorLight: '#fff'
        });
    },


    /**
     * Creates a new modal using content specified in the modals object.
     * @param {string} id - The key to reference in modals object
     * @param {string} [content] - Used if content doesn’t otherwise exist
     */

    createModal(id, content = `<h3>${id}</h3>`) {
        //console.log(`createModal(${id})`);
        el.modals[id] = picoModal({
            modalId: 'modal-' + id,
            closeButton: false,
            content: modals[id].content || content
        })
        .afterShow(modal => {
            modal.overlayElem().classList.add('show');
            modal.modalElem().classList.add('show');
        })
        .beforeClose((modal, ev) => {
            ev.preventDefault();
            modal.overlayElem().classList.remove('show');
            modal.modalElem().classList.remove('show');
            setTimeout(modal.forceClose, 700);
        });
        if (typeof modals[id].afterCreate === 'function') {
            el.modals[id].afterCreate(modals[id].afterCreate);
        }
        if (typeof modals[id].afterClose === 'function') {
            el.modals[id].afterClose(modals[id].afterClose);
        }
    },


    /**
     * Destroys a modal if it exists.
     * @param {string} id - The key to reference in modals object
     */

    destroyModal(id) {
        //console.log(`destroyModal(${id})`);
        if (el.modals[id]) {
            el.modals[id].destroy();
            delete el.modals[id];
        }
    },


    /**
     * Shows a modal, first creating it if necessay.
     * @param {string} id - The key to reference in modals object
     */

    showModal(id) {
        //console.log(`showModal(${id})`);
        el.modals[id] || pc.createModal(id);
        el.modals[id].show();
    },


    /**
     * Gets data for current compass from the server.
     */

    fetchCompass() {
        //console.log('fetchCompass()');
        axios.get('/api/' + pc.id)
            .then(res => {
                //console.log(res);
                if (res.data && res.data.ts) {
                    if (res.data.ts > pc.userData.ts) {
                        pc.userData = res.data;
                        pc.storeUserData(res.data);
                        pc.clearUserDefined();
                        pc.addUserDefined();
                        pc.addNewPointMenuItem();
                        if (location.search) {
                            const user = pc.getUrlParameters();
                            user && pc.togglePoints(user, true);
                        }
                        pc.setTlCheckboxAndUrlParam('user');
                        pc.setButtons(res.data.arc);
                    } else if (res.data.ts < pc.userData.ts) {
                        pc.addNewPointMenuItem();
                        pc.sendCompass();
                    }
                }
            })
            .catch(err => {
                console.log(err);
                if (err.response.status === 404) {
                    pc.showModal('notFound');
                } else {
                    pc.showModal('fetchError');
                }
                history.replaceState(null, '', '/');
                pc.id = 'new';
                pc.hideButtons('edit', 'archive', 'new');
            });
    },


    /**
     * Posts data for current compass to the server and prompts if there is a conflict.
     */

    sendCompass() {
        //console.log('sendCompass()');
        axios.post('/api/' + pc.id, pc.userData)
            .then(res => {
                //console.log(res);
                if (res.data) {
                    if (res.data.id) {
                        pc.id = res.data.id;
                        history.replaceState(null, '', `/${res.data.id}/${location.search}`);
                        pc.storeUserData();
                        localStorage && localStorage.removeItem('new');
                        pc.isWaitingForId = false;
                        pc.destroyModal('share');
                        el.buttons.archive.classList.add('show');
                        pc.showModal('saved');
                    } else if (res.data.arc) {
                        pc.serverData = res.data;
                        pc.showModal('is_arc');
                    } else if (res.data.ts && res.data.ts > pc.userData.ts) {
                        pc.serverData = res.data;
                        pc.showModal('conflict');
                    }
                }
            })
            .catch(err => {
                console.log(err);
                pc.isWaitingForId = false;
                err.response.status === 404 || pc.showModal('problem');
            });
    },


    /**
     * Puts current compass in a state where it cannot be edited.
     */

    archiveCompass() {
        //console.log('archiveCompass()');
        pc.userData.arc = true;
        pc.storeUserData();
        navigator.onLine && pc.sendCompass();
        pc.isArc = true;
        el.buttons.edit.classList.remove('show');
        el.buttons.archive.classList.add('show', 'active');
        const addItem = el.userSubMenu.querySelector('.add-item');
        addItem && addItem.remove();
        pc.destroyModal('open');
        pc.destroyModal('share');
    },


    /**
     * Copies data for current compass to a new compass ID.
     * Updates interface to allow editing new compass.
     */

    forkCompass() {
        //console.log('forkCompass()');
        pc.id = 'new';
        pc.storeUserData();
        pc.isWaitingForId = true;
        if (navigator.onLine) {
            pc.sendCompass();
        } else {
            pc.hideButtons('archive', 'fork', 'share');
        }
        pc.isArc = false;
        el.buttons.archive.classList.remove('active');
        el.buttons.edit.classList.add('show');
        pc.addNewPointMenuItem();
        pc.destroyModal('open');
        pc.destroyModal('share');
    },


    /**
     * Clears the current compass and sets up a new one for editing.
     */

    newCompass() {
        //console.log('newCompass()');
        pc.clearUserDefined();
        pc.resetPointValues();
        el.topBar.classList.remove('show');
        pc.hideButtons('archive', 'fork', 'new', 'share');
        el.buttons.archive.classList.remove('active');
        el.buttons.archive.title = 'Archive';
        pc.isWaitingForId = false;
        pc.id = 'new';
        pc.userData = pc.defData;
        localStorage && localStorage.new && localStorage.removeItem('new');
        history.pushState(null, '', '/');
        el.tlCheckboxes.user.checked = false;
        pc.addNewPointMenuItem();
    },


    /**
     * Checks if a compass ID is valid.
     * @param {string} id - The 'shortid' to validate
     * @return {boolean}
     */

    validateId(id) {
        //console.log(`validateId(${id})`);
        if (typeof id === 'string' && id.length > 6) {
            const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-';
            for (let i = 0; i < id.length; i++) {
                if (chars.indexOf(id[i]) === -1) {
                    id = false;
                    break;
                }
            }
        } else {
            id = false;
        }
        return id;
    }

};
