/* globals d3, points, el, pc */
'use strict';

pc.renderCompass = () => {
    //console.log('renderCompass()');

    // Create svg inside main element
    el.svg = d3.select(el.main)
        .append('svg')
        .attr('width', pc.dim * 10)
        .attr('height', pc.dim * 10)
        .call(el.tip);

    // Create compass element inside svg
    el.svg.append('g')
        .classed('compass', true)
        .attr('transform', `translate(${pc.dim}, ${pc.dim})`)
        .attr('width', pc.dim * 8)
        .attr('height', pc.dim * 8);

    el.compass = d3.select('svg g.compass');

    // Render labels that appear outside quadrants
    el.labels.auth = el.compass.append('text')
        .classed('compass-label', true)
        .classed('direction-label', true)
        .attr('id', 'label-auth')
        .attr('x', pc.dim * 4)
        .attr('y', pc.dim * 0.25)
        .attr('text-anchor', 'middle')
        .text('Authoritarian');

    el.labels.lib = el.compass.append('text')
        .classed('compass-label', true)
        .classed('direction-label', true)
        .attr('id', 'label-lib')
        .attr('x', pc.dim * 4)
        .attr('y', pc.dim * 8)
        .attr('text-anchor', 'middle')
        .text('Libertarian');

    el.labels.left = el.compass.append('text')
        .classed('compass-label', true)
        .classed('direction-label', true)
        .attr('id', 'label-left')
        .attr('x', 0)
        .attr('y', (pc.dim * 4) + (pc.rem * 0.4))
        .attr('text-anchor', 'middle')
        .text('Left');

    el.labels.right = el.compass.append('text')
        .classed('compass-label', true)
        .classed('direction-label', true)
        .attr('id', 'label-right')
        .attr('x', pc.dim * 7.75)
        .attr('y', (pc.dim * 4) + (pc.rem * 0.4))
        .attr('text-anchor', 'right')
        .text('Right');

    el.labels.communism = el.compass.append('text')
        .classed('compass-label', true)
        .classed('ideology-label', true)
        .attr('transform', 'rotate(-90)')
        .attr('id', 'label-communism')
        .attr('x', pc.dim * -4)
        .attr('y', pc.dim - pc.rem)
        .attr('text-anchor', 'middle')
        .text('communism');

    el.labels.fascism = el.compass.append('text')
        .classed('compass-label', true)
        .classed('ideology-label', true)
        .attr('id', 'label-fascism')
        .attr('x', pc.dim * 4)
        .attr('y', pc.dim - pc.rem)
        .attr('text-anchor', 'middle')
        .text('fascism');

    el.labels.neoliberalism = el.compass.append('text')
        .classed('compass-label', true)
        .classed('ideology-label', true)
        .attr('transform', 'rotate(90)')
        .attr('id', 'label-neoliberalism')
        .attr('x', pc.dim * 4)
        .attr('y', pc.dim * -7 - pc.rem)
        .attr('text-anchor', 'middle')
        .text('neoliberalism');

    el.labels.anarchism = el.compass.append('text')
        .classed('compass-label', true)
        .classed('ideology-label', true)
        .attr('id', 'label-anarchism')
        .attr('x', pc.dim * 4)
        .attr('y', pc.dim * 7 + (pc.rem * 1.5))
        .attr('text-anchor', 'middle')
        .text('anarchism');

    // Render colored quadrants on compass
    el.quadrants.authLeft = el.compass.append('rect')
        .classed('quadrant', true)
        .attr('id', 'quadrant-auth-left')
        .attr('x', pc.dim)
        .attr('y', pc.dim)
        .attr('width', pc.dim * 3)
        .attr('height', pc.dim * 3);

    el.quadrants.authRight = el.compass.append('rect')
        .classed('quadrant', true)
        .attr('id', 'quadrant-auth-right')
        .attr('x', pc.dim * 4 + 1)
        .attr('y', pc.dim)
        .attr('width', pc.dim * 3)
        .attr('height', pc.dim * 3);

    el.quadrants.libLeft = el.compass.append('rect')
        .classed('quadrant', true)
        .attr('id', 'quadrant-lib-left')
        .attr('x', pc.dim)
        .attr('y', pc.dim * 4 + 1)
        .attr('width', pc.dim * 3)
        .attr('height', pc.dim * 3);

        el.quadrants.libRight = el.compass.append('rect')
        .classed('quadrant', true)
        .attr('id', 'quadrant-lib-right')
        .attr('x', pc.dim * 4 + 1)
        .attr('y', pc.dim * 4 + 1)
        .attr('width', pc.dim * 3)
        .attr('height', pc.dim * 3);

    pc.setScales();

    // Render axes on compass
    el.xAxis = el.compass.append('g')
        .classed('axis', true)
        .attr('transform', `translate(0, ${pc.dim * 4})`)
        .attr('id', 'x-axis')
        .attr('stroke-linejoin', 'round')
        .call(pc.makeAxis);

    el.yAxis = el.compass.append('g')
        .classed('axis', true)
        .attr('transform', `translate(${pc.dim * 4}, 0)`)
        .attr('id', 'y-axis')
        .attr('stroke-linejoin', 'round')
        .call(pc.makeAxis);

    // Fix tick positions on axes
    el.xAxis.selectAll('.tick').each(function () {
        const tick = d3.select(this),
             value = tick.attr('transform').replace(/0?\)/, `${pc.dim * -3})`);
        tick.attr('transform', value);
    });

    el.yAxis.selectAll('.tick').each(function () {
        const tick = d3.select(this),
             value = tick.attr('transform').replace('(0', `(${pc.dim * -3}`);
        tick.attr('transform', value);
    });

    // Render labels that appear on top of quadrants
    el.labels.economic = el.compass.append('text')
        .classed('compass-label', true)
        .classed('axis-label', true)
        .attr('transform', `translate(${pc.dim * 1.9}, ${pc.dim * 4 - 3})`)
        .attr('id', 'label-economic')
        .attr('text-anchor', 'middle')
        .text('Economic');

    el.labels.social = el.compass.append('text')
        .classed('compass-label', true)
        .classed('axis-label', true)
        .attr('transform', `rotate(-90) translate(${pc.dim * -6.45}, ${pc.dim * 4 - 3})`)
        .attr('id', 'label-social')
        .attr('text-anchor', 'middle')
        .text('Social');

    // Render points on compass
    el.points = el.compass.selectAll('circle')
        .data(points)
        .enter()
        .append('circle')
        .attr('class', d => 'point fill-' + d.color)
        .attr('id', d => 'point-' + d.name)
        .attr('cx', d => pc.xScale(d.x))
        .attr('cy', d => pc.yScale(d.y))
        .attr('r', pc.rem * 0.5)
        .on('mouseover', el.tip.show)
        .on('mouseout', el.tip.hide)
        .on('selectpoint', el.tip.show)
        .on('deselectpoint', el.tip.hide);

    // Render tooltips above compass points
    el.points.append('rect');

    // Set visibility of points according to checkboxes
    [...document.querySelectorAll('.sub-menu input[type="checkbox"]')].forEach(checkbox => {
        el.compass.select('#point-' + CSS.escape(checkbox.name))
            .classed('show', checkbox.checked);
    });

};
