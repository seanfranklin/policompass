'use strict';

const points = [

    // People
    {  x:   8.0,     y:   5.0,     name: 'Bush',           color: 'gop-red'     },
    {  x:   7.5,     y:   5.0,     name: 'Clinton',        color: 'dem-blue'    },
    {  x:   9.0,     y:  -2.0,     name: 'Friedman',       color: 'reg-purple'  },
    {  x:  -7.0,     y:  -3.5,     name: 'Gandhi',         color: 'reg-green'   },
    {  x:   1.5,     y:   9.5,     name: 'Hitler',         color: 'nazi-red'    },
    {  x:   6.0,     y:   6.0,     name: 'Obama',          color: 'dem-blue'    },
    {  x:  -2.0,     y:   0.0,     name: 'Sanders',        color: 'dem-blue'    },
    {  x:  -9.5,     y:   9.0,     name: 'Stalin',         color: 'soviet-red'  },
    {  x:   1.0,     y:   7.0,     name: 'Thatcher',       color: 'tory-blue'   },
    {  x:   6.5,     y:   9.0,     name: 'Trump',          color: 'reg-orange'  },

    // Parties
    {  x:   9.0,     y:   6.5,     name: 'Conservative',   color: 'tory-blue'   },
    {  x:  -4.0,     y:  -5.0,     name: 'Green',          color: 'tgp-green'   },
    {  x:   4.0,     y:   5.5,     name: 'Labour',         color: 'labour-red'  },
    {  x:  -1.0,     y:   1.5,     name: 'SNP',            color: 'snp-yellow'  },
    {  x:   8.0,     y:   8.0,     name: 'UKIP',           color: 'ukip-purple' }

];
