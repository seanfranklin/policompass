'use strict';

const cacheName = 'pc_cache_v2',

    assets = [
        '/',
        'index.html',
        'style.min.css',
        'fonts/Istok.woff2',
        'fonts/fa.woff2',
        'scripts/axios.min.js',
        'scripts/css.escape.min.js',
        'scripts/d3.v4.min.js',
        'scripts/d3.tip.min.js',
        'scripts/hammer.min.js',
        'scripts/picomodal.min.js',
        'scripts/qrcode.min.js',
        'scripts/sortable.min.js',
        'scripts/load.min.js',
        'scripts/pc.min.js'
    ],

    updateable = [
        'index.html',
        'style.min.css',
        'scripts/load.min.js',
        'scripts/pc.min.js'
    ];


/**
 * Open versioned cache from the promise-based caches API.
 * Request assets for offline use and add them to cache.
 */

self.addEventListener('install', ev => {
    //console.info('Event: install service worker');
    ev.waitUntil(caches.open(cacheName).then(cache => cache.addAll(assets)));
});


/**
 * Activation fails unless promise is resolved to an array of available cache keys.
 * Filter by keys that don't match cacheName and delete old caches.
 */

self.addEventListener('activate', ev => {
    //console.info('Event: activate service worker');
    ev.waitUntil(caches.keys().then(keys => {
        return Promise.all(keys.filter(key => key !== cacheName)
            .map(key => caches.delete(key)));
    }).then(self.clients.claim()));
});


/**
 * Intercept request for asset and check for match in cache.
 * Return cached response or fall back to waiting for remote server.
 */

self.addEventListener('fetch', ev => {
    const req = ev.request,
        asset = req.url.substring(24),

    updateCache = res => {
        caches.open(cacheName).then(cache => cache.add(res.clone()));
        console.info(asset + ' updated in cache');
        return res;
    },

    getFromServer = () => {
        return fetch(req).then(res => {
            console.info(asset + ' fetched from server');
            return res;
        }).catch(() => {
            console.warn(`Failed to fetch ${asset} from server`);
        });
    };

    console.info('Event: fetch ' + asset);
    if (req.method === 'GET' && assets.includes(asset)) {
        console.info(asset + ' should be cached');
        ev.respondWith(caches.match(req).then(cached => {
            if (cached) {
                console.info(asset + ' found in cache');
                if (navigator.onLine && updateable.includes(asset)) {
                    console.info(asset + ' is worth updating');
                    fetch(req).then(updateCache);
                }
            }
            return cached || getFromServer().then(updateCache);
        }));
    } else {
        console.info(asset + ' should not be cached');
        ev.respondWith(getFromServer());
    }
});
